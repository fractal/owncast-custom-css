# Custom CSS for Owncast

## main.css

CSS for the web front end (user side), with some tweaks to make it look more like Twitch (especially the chat).

The content of the file is to be pasted under "Customize your page styling with CSS" in your Owncast instance's admin area : https://your-domain.tld/admin/config-public-details/.

![Screenshot of Owncast with the custom CSS applied.](main-css.png)

# chat-overlay-obs.css

CSS to be used when showing the chat on stream.
 Based on the excellent code by Hatnix (https://github.com/hatniX/Owncast-tweaks/blob/main/Owncast_onscreen_chat_fade.css)!


1. Add a "browser" source to OBS pointing to your instance's readonly chat (https://your-domain.tld/embed/chat/readonly).
2. In the source's properties, paste the content of the file into the "Custom CSS" dield.